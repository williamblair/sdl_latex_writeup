/* Buffer for the controller */
static unsigned short padbuf = 0;
/*  keep track of the last read for SDL events */
static unsigned short prev_padbuf = 0; 

static char leftanalog[2] = { 0, 0 }; // x and y
static char leftanalog_prev[2] = { 0, 0 }; // previous x and y

static char rightanalog[2] = { 0, 0 }; // x and y
static char rightanalog_prev[2] = { 0, 0 }; // previous x and y

/* holds more advanced pad info (ie joystick) */
static psx_pad_state pad_state; 
...
int SDL_PollEvent (SDL_Event *event)
{
    const int ANALOG_THRESH = 30; // how much the analog has to be
                                  // moved to register as an event
    
    /* Update the pad buffers */
    PSX_PollPad(0, &pad_state);
    prev_padbuf = padbuf;
    padbuf = pad_state.buttons;
    
    /* values are from -128 to 127 */
    leftanalog_prev[0] = leftanalog[0];
    leftanalog_prev[1] = leftanalog[1];
    
    leftanalog[0] = pad_state.extra.analogJoy.x[1];
    leftanalog[1] = pad_state.extra.analogJoy.y[1];
    
    rightanalog_prev[0] = rightanalog[0];
    rightanalog_prev[1] = rightanalog[1];
    
    rightanalog[0] = pad_state.extra.analogJoy.x[0];
    rightanalog[1] = pad_state.extra.analogJoy.y[0];
    
/*
 * Analog Sticks
 */
    // Left analog X
    if ((leftanalog[0] > ANALOG_THRESH || 
         leftanalog[0] < -ANALOG_THRESH) &&
         leftanalog[0] != leftanalog_prev[0]) {
        
        printf("left Analog X Event\n");
        
        event->jaxis.type = SDL_JOYAXISMOTION;
        event->jaxis.axis = 0; // 0 = x axis left
        event->jaxis.which = 0; // always 0 for first controller
        /* map from -127..128 to -32768..32767 */
        event->jaxis.value = leftanalog[0] * 255; 
        
        return 1;
    } 
    ...
    /* If the pad buttons state hasn't changed since the last check,
     * report nothing 
     */
    if (padbuf == prev_padbuf) {
        return 0;
    }
    
    /* intiailize value for now, since the buttons OR it... */
    event->type = 0;
    
/*
 * Hat / Directional buttons
 */ 
    // UpLeft
    if ((padbuf & PAD_LEFT) && (padbuf & PAD_UP)) {
        printf("Upleft Event!\n");
        event->type = SDL_JOYHATMOTION;
        event->jhat.hat = 0;
        event->jhat.value = SDL_HAT_LEFTUP;
        event->jhat.which = 0;
    }
    ...
/*
 * Main buttons (square/triangle/circle/cross)
 */
    // cross
    if ((padbuf & PAD_CROSS) && !(prev_padbuf & PAD_CROSS)) {
        printf("Cross Pressed event!\n");
        event->type |= SDL_JOYBUTTONDOWN;
        event->jbutton.type = SDL_JOYBUTTONDOWN;
        event->jbutton.button = 0; // X == 0
        event->jbutton.state = SDL_PRESSED;
        event->jbutton.which = 0; // joystick index 0
    }
    else if (!(padbuf & PAD_CROSS) && (prev_padbuf & PAD_CROSS)) {
        printf("Cross Released event!\n");
        event->type |= SDL_JOYBUTTONUP;
        event->jbutton.type = SDL_JOYBUTTONUP;
        event->jbutton.button = 0; // X == 0
        event->jbutton.state = SDL_RELEASED;
        event->jbutton.which = 0; // joystick index 0
    }
    ...
    return 1;
}
