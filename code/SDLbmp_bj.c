/* Max texture width of a sprite */
#define MAX_SPR_SIZE 255

typedef struct {

    uint16_t header; // the header field
    uint32_t filesize; // size of the file in bytes

    /* 4 bytes reserved data (based on the  program that creates it) */
    uint32_t reserved; 

    /* 4 bytes offset (starting address) of where pixels are in file */
    uint32_t pix_offset; 

} BMP_header;

/*
 * Load BMP (assumes is 16bit)
 */
static void load_bmp(SDL_RWops *src, uint16_t *buffer, int width, int height)
{
    int i,j;
    uint8_t r,g,b;
    /* holds the pixels from the bmp, before we process them */
    uint16_t pixel_buffer[(MAX_SPR_SIZE+1)*S_HEIGHT];
    BMP_header header;

    uint32_t image_width = width, image_height = height;
    uint16_t bits_per_pixel = 16;
    uint32_t bytes_read;
    /* the image width aligned/padded to the nearest 4 bytes */
    uint32_t padded_width; 

    /* Read in the BMP header */
    SDL_RWread(src, &header.header, 2, 1); // Read in the header
    SDL_RWread(src, &header.filesize, 4, 1); // Read in the file size
    SDL_RWread(src, &header.reserved, 4, 1); // Read in reserved
    SDL_RWread(src, &header.pix_offset, 4, 1); // Read in pixel offset

    /* Go straight to the pixels */
    SDL_RWseek(src, header.pix_offset, RW_SEEK_SET);

    /* When reading, the BMP pads the width to 4 bytes
     * align to 2 instead of 4 since uint16 = 2 bytes each
     */
    padded_width = image_width+(image_width%2);
    printf("Padded width: %d\n", padded_width);

    if (bits_per_pixel != 16) {
        printf("!!!!!UNIMPLEMENTED BITS PER PIXEL!!!!!\n");
        printf("  Bits Per Pixel: %d\n", bits_per_pixel);
        return;
    }

    /* read in the pixel data */
    bytes_read = SDL_RWread(src, pixel_buffer, 2, padded_width*image_height);
    printf("Read %d bytes\n", bytes_read);

    /* now we need to flip Y, and swap the R and B components */
    for (i = 0; i < image_width; ++i) {
        for (j = 0; j < image_height; ++j) {

            /* get the pixel at the flipped y coordinate */
            uint16_t curpix = pixel_buffer[(image_height-1-j)*padded_width+i];

            /* Extract each color from the pixel */
            b = curpix & 31;
            g = (curpix>>5)&31;
            r = (curpix>>10)&31;
    
            /* set the new color (switch b and r)
             * also y is flipped compared to curpix
             */
            buffer[j*image_width+i] = (b<<10) | (g<<5) | r;
        }
    }
}
