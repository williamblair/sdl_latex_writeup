 if (biBitCount == 8) {

    /* If the image doesn't have an even width we need to pad it
     * this is because we're uploading to aligned 2 bytes/16 bits 
     */
    if (surface->w & 1) {
        Uint8 *test_pixels = (Uint8*)malloc((surface->w+1)*surface->h);
        for (i=0; i<surface->w; ++i) {
            for (j=0; j<surface->h; ++j) {
                test_pixels[j*(surface->w+1)+i] = 
                    ((Uint8*)surface->pixels)[j*surface->w+i];
            }
        }

        /* upload as 'half' the width since each pixel is 1byte instead of 2 as psx expects */
        LoadImage(test_pixels, vram_tex_x, vram_tex_y, 
            (surface->w+1)>>1, surface->h);

        free(test_pixels);

        vram_tex_x += (surface->w+1)/2;
    } else {
        LoadImage(surface->pixels, vram_tex_x, vram_tex_y, 
            (surface->w)>>1, surface->h);
        vram_tex_x += surface->w/2;
    }

    printf("Loading palette!\n");
    Uint16 *psx_pal = (Uint16*)malloc(biClrUsed*sizeof(Uint16));
    for (i=0; i<biClrUsed; ++i) {
        psx_pal[i] = gs_rgb_to_psx((palette->colors[i].r), 
            (palette->colors[i].g), (palette->colors[i].b));
    }

    LoadImage(psx_pal, vram_clut_x, vram_clut_y, biClrUsed, 1);
    vram_clut_y++;
    
    spr->sprite.attribute = COLORMODE(COLORMODE_8BPP);
    spr->sprite.cx = vram_clut_x;
    spr->sprite.cy = vram_clut_y-1;

    spr->sprite.w = surface->w;

    free(psx_pal);

    printf("Vram tex x before, tpage x, width: %d,%d,%d\n", 
        vram_tex_x, vram_tpage_x, surface->w);
}
