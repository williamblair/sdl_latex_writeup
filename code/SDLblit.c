/* pairs an SDL surface with a psx-sdk sprite,
 * to map a surface with a psx vram sprite texture
 */
typedef struct {
    GsSprite    sprite;
    SDL_Surface *surface;
} SpriteKey;

/*
 * Keep track of all of the surfaces allocated
 */
extern uint32_t sdl_surface_count;
#define MAX_SPRITES 10
extern SpriteKey sdl_sprites[MAX_SPRITES];

/* This is done by SDL in SDL_video.h */
#define SDL_BlitSurface SDL_UpperBlit

int SDL_UpperBlit (SDL_Surface *src, SDL_Rect *srcrect,
           SDL_Surface *dst, SDL_Rect *dstrect)
{
    SDL_Rect fulldst;
    int srcx, srcy, w, h;
    int i;

    /* Make sure the surfaces aren't locked */
    if ( ! src || ! dst ) {
        SDL_DebugPrint("SDL_UpperBlit: passed a NULL surface");
        return(-1);
    }
    if ( src->locked || dst->locked ) {
        SDL_DebugPrint("Surfaces must not be locked during blit");
        return(-1);
    }

    /* If the destination rect is NULL, use the entire dest surface */
    if ( dstrect == NULL ) {
        fulldst.x = fulldst.y = 0;
        dstrect = &fulldst;
    }
    
    /* 
     * PSX Impl - check if the surface is in the list of loaded sprites
     */
    for (i=0; i<sdl_surface_count; ++i) {
        
        /* Check if pointers match */
        if (sdl_sprites[i].surface == src) {

            /* Set the X and Y of the sprite */
            if (dstrect) {
                sdl_sprites[i].sprite.x = dstrect->x;
                sdl_sprites[i].sprite.y = dstrect->y;
            }

            /*
             * Set the u/v texpage clip
             */
            if (srcrect) {
                sdl_sprites[i].sprite.u = srcrect->x;
                sdl_sprites[i].sprite.v = srcrect->y;
                sdl_sprites[i].sprite.w = srcrect->w;
                sdl_sprites[i].sprite.h = srcrect->h;
            }

            /* Do the 'blitting', which in this case we are 
             * just telling the PSX we want to draw it in this order
             */
            GsSortSprite(&sdl_sprites[i].sprite);

            /* Skip the rest of the function */
            return 0;
        }
    }
    ...
