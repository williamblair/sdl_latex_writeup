#include <stdio.h>
#include <SDL/SDL.h>

#define SCREEN_WIDTH   640
#define SCREEN_HEIGHT  480
#define BITS_PER_PIXEL 32

int main(int argc, char *argv[])
{
    int quit = 0;
    SDL_Event event;
    SDL_Surface *screen = NULL;

    /* Initialize SDL subsystems */
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        
        fprintf(stderr, "Failed to init SDL: %s\n",
            SDL_GetError());

        return -1;
    }

    /* Create the program window */
    screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT,
            BITS_PER_PIXEL, SDL_HWSURFACE);

    do
    {
        /* Look for events */
        while(SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT) {
                quit = 1;
            }
        }

        /* Update the program window */
        SDL_Flip(screen);

        /* Limit the program to 30 fps */
        SDL_Delay(1000.0f/30.0f);

    } while (!quit);

    /* Free memory and quit subsystems */
    SDL_FreeSurface(screen);
    SDL_Quit();

    return 0;
}

