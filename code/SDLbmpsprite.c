        ...
        /* Create a PSX Sprite to use for this surface */
        SpriteKey *spr = &sdl_sprites[sdl_surface_count];
        
        /*
        * See if our new texture fits in the current vram position
        */
        if (vram_tex_x + surface->w > 1024) {
            vram_tex_x = S_WIDTH;
            vram_tex_y = 256;

            vram_tpage_x = vram_tex_x/64;
            vram_tpage_y = 1;
        }

        /* Center on the screen with offset */
        spr->sprite.x = 0;
        spr->sprite.y = 0;
        spr->sprite.w = surface->w; 
        spr->sprite.h = surface->h;
        spr->sprite.u = spr->sprite.v = 0;
        spr->sprite.cx = spr->sprite.cy = 0;

        spr->sprite.r = spr->sprite.g = spr->sprite.b = NORMAL_LUMINOSITY;

        spr->sprite.scalex = spr->sprite.scaley = 1;
        spr->sprite.mx = spr->sprite.my = 0;

        spr->sprite.attribute = COLORMODE(COLORMODE_16BPP);

        /* Set the new calculated tpage of the sprite */
        spr->sprite.tpage = vram_tpage_x+(16*vram_tpage_y);

        /* set the surface pointer that maps to the GsSprite */
        sdl_sprites[sdl_surface_count].surface = surface;

        printf("Setting spr surface: 0x%X\n", spr->surface);
        ...
            /* upload the image to vram */
            LoadImage(surface->pixels, vram_tex_x, vram_tex_y, surface->w, surface->h);
            printf("VRam tex x, y: %d, %d\n", vram_tex_x, vram_tex_y);
            
            /*
             * Update the vram texture locations
             */
            printf("Vram tex x before, tpage x, width: %d,%d,%d\n", vram_tex_x, vram_tpage_x, surface->w);
            vram_tex_x += surface->w;
        }
        
        /*  texture pages must be 64pixel aligned*/
        if (vram_tex_x % 64) vram_tex_x += 64 - (vram_tex_x % 64); 
        /*  this should theoretically always line up... */
        vram_tpage_x = vram_tex_x / 64; 
        printf("vram_tex_x after, tpage: %d\n", vram_tex_x, vram_tpage_x);

        /* increase the total number of surfaces */
        sdl_surface_count++;
        printf("Surface Count: %d\n", sdl_surface_count);
    }
    ...
