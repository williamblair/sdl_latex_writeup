SDL_Surface *SDL_LoadBMP_RW(SDL_RWops *src, int freesrc)
{
    Uint8 *bj_buff = NULL;
    Uint16 bj_buff_size = 0;
    Uint16 bj_buff_index = 0;

    /* Make sure we are passed a valid data source */
    surface = NULL;
    was_error = SDL_FALSE;
    if ( src == NULL ) {
        was_error = SDL_TRUE;
        goto done;
    }
    
    fp_offset = SDL_RWtell(src);
    
    /* Read the file into a buffer */
    SDL_RWseek(src, 0, RW_SEEK_END);
    bj_buff_size = SDL_RWtell(src);
    SDL_RWseek(src, 0, RW_SEEK_SET);
    bj_buff = (Uint8*)SDL_malloc(bj_buff_size);
    if (SDL_RWread(src, bj_buff, 1, bj_buff_size) != bj_buff_size) {
        SDL_DebugPrint("Failed to read into bj buffer!");
        SDL_free(bj_buff);
        was_error = SDL_TRUE;
        goto done;
    }
    
    /* Move back to the beginning of the file for bj 16bmp load */
    SDL_RWseek(src, 0, RW_SEEK_SET);

    /* Read in the BMP file header */
    bj_buff_index = fp_offset;
    
    memcpy(magic, &bj_buff[bj_buff_index], 2);
    bj_buff_index += 2;

    if ( SDL_strncmp(magic, "BM", 2) != 0 ) {
        SDL_DebugPrint("File is now a Windows BMP file");
        was_error = SDL_TRUE;
        goto done;
    }
    memcpy(&bfSize, &bj_buff[bj_buff_index], 4);
    bj_buff_index += 4;

    ...
    
    if (biBitCount == 16) {
        printf("16bit! using bj function!\n");
        load_bmp(src, (Uint16*)surface->pixels, biWidth, biHeight);
        goto done;
    }

    ...
    
done:
    if ( was_error ) {
        if ( src ) {
            SDL_RWseek(src, fp_offset, RW_SEEK_SET);
        }
        if ( surface ) {
            SDL_FreeSurface(surface);
        }
        surface = NULL;
    }
    ...
