% Chapter Template

\chapter{The SONY PlayStation\textsuperscript{\textregistered} 1} % Main chapter title

\label{Chapter2} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

%----------------------------------------------------------------------------------------
%	SECTION 1 - History
%----------------------------------------------------------------------------------------

\section{History}

SONY's PlayStation\textsuperscript{\textregistered} 1 is a video game console which was released in Japan
on December 3rd, 1994, and in the U.S. on September 9th, 1995 \cite{Everything}.
Its creation was the child of a failed business deal between SONY and Nintendo,
in which SONY was to create a CD-ROM extension for the Super NES. After
Nintendo announced they would instead partner with Philips for their CD-ROM,
SONY decided to instead create their own console, dubbed the PlayStation\textsuperscript{\textregistered} X.
The PSX has 7,918 games (including different titles from different regions)
\cite{Titles}.
As of March 2007 the PSX has sold 102.49 million units worldwide \cite{Units}.

%----------------------------------------------------------------------------------------
%	SECTION 2 - Hardware
%----------------------------------------------------------------------------------------

\section{System Overview/Hardware Specifications}
\label{sysoverview}

The PlayStation\textsuperscript{\textregistered} 1 contains a 32 bit MIPS CPU, a 32 bit GPU and 16 bit SPU
(both designed by SONY), 1MB RAM/512KB BIOS ROM, a custom CPU coprocessor for 
vector and matrix multiplication (called the Geometry 
Transoformation Engine (GTE)), a Motion Decoder (MDEC) for streaming 
video, two controller and memory card 
ports, a CD-ROM drive supporting CD-Digital Audio, and a serial and parallel I/O port
(these were removed in newer, smaller console versions). A diagram of the PlayStation\textsuperscript{\textregistered}'s
hardware layout is shown in figure \ref{fig:psx_circuit}.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{images/psx_circuit.png}
\caption{PSX hardware Layout \cite{NEXT6}}
\label{fig:psx_circuit}
\end{figure}

Upon boot, the bios searches the CD-ROM (if inserted) for a file named 
\textit{system.cnf}. This file contains the following information \cite{NOPSX}:

\begin{enumerate}
  \item BOOT - the name of the executable for the PSX to run
  \item TCB - the maximum number of threads
  \item EVENT - the maximum number of events
  \item STACK - memory location to use for the stack pointer
\end{enumerate}

If a CD is not inserted, the BIOS menu system will load, which allows you to manage
memory card data (e.g. game saves) or access the CD player, which will play audio 
CDs.


%----------------------------------------------------------------------------------------

\subsection{CPU}

PlayStation\textsuperscript{\textregistered}'s CPU is a modified 32 bit MIPS R3000A Reduced Instruction
Set Controller (RISC) CPU. It runs at 33.8688 MHz and contains a 4 KB
L1 cache. The chip was developed in conjunction with LSI Logic Corp and
SGI \cite{Everything}. Both the GTE and MDEC reside within the CPU.

\subsection{MDEC}

The Motion Decoder decompresses image and video and can send these
images directly to the GPU via DMA transfer \cite{NEXT}. The images
are sent as 24 bit 16x16 pixels at a time, called 'Macroblocks'
\cite{Everything}. Its speed is 80 Million Instructions per Second,
and is capable of streaming a resolution of 320x240 pixels at about
30 Frames Per Second \cite{Everything}. This is the most common use
of the GPU's 24 bit display mode.

\subsection{GTE}

The Geometry Transformation Engine (GTE) is a coprocessor in the CPU which does 
matrix and vector math, mainly for 3D graphics. It is capable of 66 Million Instructions
per Second (MIPS), and can render 1.5 million flat-shaded polygons per second (500,000
if texture mapped and light sourced) \cite{NEXT}. The GTE is called and used
via special assembly instructions. For example, some of these instructions are
RTPT (Rotate, Translate, and Perspective Transformation), MVMVA (Matrix Vector
Multiplication and Addition), and DCPL (Depth Cue Light Color) \cite{hitmen}.
There are a total of 64 data and control registers specific in the GTE, each of size
32 bits, which are used to store data from/for GTE operations \cite{hitmen}.

%----------------------------------------------------------------------------------------

\subsection{GPU \& Video RAM}
\label{gpuoverview}

The GPU of the PSX is a 32 bit chip which manages the graphical output of the PlayStation\textsuperscript{\textregistered}. 
It has access to 1MB of Video RAM (VRAM), which is represented as 
1024x512 px (pixels) at 16bpp (bits per pixel), and is comprised
of 32 texture pages of size 64x256 px each. Pixels in VRAM are composed of 1 masking
bit, followed by 5 bits each of blue, green, then red (figure \ref{fig:vram_format}).

\begin{figure}[h]
    \centering
    \begin{bytefield}[bitheight=\widthof{~Mask~},
    boxformatting={\centering\small}]{16}
    \bitheader[endianness=big]{15,14,10,5,0} \\
    \bitbox{1}{\rotatebox{90}{Mask}} &
    \colorbitbox{lightcyan}{5}{Blue} &
    \colorbitbox{lightgreen}{5}{Green} &
    \colorbitbox{lightred}{5}{Red}
    \end{bytefield}
    \caption{PSX VRAM Pixel Format}
    \label{fig:vram_format}
\end{figure}

The VRAM is not directly accessible to the programmer and is instead managed via
commands sent to the GPU. For example, the GPU opcode for copying
data from CPU to VRAM is 0xA0 \cite{NOPSX}. Textures stored in VRAM can be any of the 
following formats: 15 bit direct, 8 bit CLUT (Color Look Up Table), or 4 bit CLUT.
For 15 bit direct textures, each pixel is stored as 1 16 bit word in vram. For 8 bit and 4 bit
CLUT modes, each pixel in VRAM corresponds to 2 or 4 pixels within the texture, and
the actual colors themselves (the CLUT) are stored elsewhere within VRAM (figure 
\ref{fig:8bit_format}). The maximum size of a texture is 256x256px,
or 4 texture pages (although, in practice, only 255px seemed to work).

\begin{figure}[h]
    \centering
    \begin{bytefield}[
    boxformatting={\centering\small}]{16}
    \bitheader[endianness=big]{15,8,0} \\
    \bitbox{8}{CLUT index 1} &
    \bitbox{8}{CLUT index 2} \\
    %\end{bytefield}

	%\begin{bytefield}[boxformatting={\centering\small}]{16}
	\bitheader[endianness=big]{15,12,8,4,0} \\
	\bitbox{4}{Index 1} &
	\bitbox{4}{Index 2} &
	\bitbox{4}{Index 3} &
	\bitbox{4}{Index 4}
	\end{bytefield}
    
    \caption{8bit (top) and 4bit (bottom) CLUT pixel mode as stored in VRAM - One pixel in VRAM stores 2 or 4 palette indices.}
    \label{fig:8bit_format}
\end{figure}

The PSX can display in either 24bpp
(8 bits per color) or 15bpp (5 bits per color). 15bpp is more common
as this basically matches the format used in VRAM. The GPU supports horizontal display resolutions
of 256, 320, 512, and 640 px, and vertical revolutions
of 240 and 480 px \cite{NOPSX}. 320x240 is the most common
resolution, as with a height of 480px you don't get to 
double buffer. If a height of 480px is used, interlacing
must be enabled in order to properly display on a t.v.
This can cause flickering, and as such is rarely used.

\begin{figure}[h]
	\begin{center}
	\begin{scaletikzpicturetowidth}{\textwidth}
    \begin{tikzpicture}[scale=\tikzscale]
        % top and bottom lines
        \draw[gray,thin] (0,0) -- (16, 0);
        \draw[gray,thin] (0,-8) -- (16, -8);

        % left and right lines
       \draw[gray, thin] (0,0) -- (0,-8);
       \draw[gray, thin] (16,0) -- (16,-8);

        % texture page grid
       \foreach \i in {1,...,15} {
             \draw[gray, very thin, dashed] (\i, 0) -- (\i, -8);
        }
        \draw[gray, very thin, dashed] (0, -4) -- (16, -4);

        % display area
        \draw[red, thin] (0,0) -- (5,0) -- (5, -7) -- (0, -7) -- (0,0);
        \draw[red, very thin] (0,-3.5) -- (5,-3.5); 

         % label the display area
         \node[draw,fill=lightgray] at (2.5,-1.5) {\textbf{Display Buffer 1}};
         \node[draw,fill=lightgray] at (2.5,-5.5) {\textbf{Display Buffer 2}};

         % label the rest
         \node[draw,fill=lightgray] at (11, -4) {\textbf{Textures/Color Palettes}};
    \end{tikzpicture}
    \end{scaletikzpicturetowidth}
    \end{center}
    \caption{Typical PSX VRAM layout. Each rectangle represents a texture page (64x256 px each)}
    \label{fig:vram}
\end{figure}

Commands sent to the GPU go either one-by-one to its data port, or to a DMA port
via a linked-list of sorts. The latter method is more efficient for large
numbers. In the linked list, each entry contains the address of the next entry and the
size of the data packet \cite{Everything}. This means that the data doesn't necessarily need
to be sent in sequential order, and that the order in which commands are process
can be changed relatively easily; in the same way you would change the order
of a linked list: simply change the address that the command you want 
to come before it points to.

However, one downside of using a linked list is the complexity of finding a specific
entry within that list. SONY's solution to this issue was to use an ordering table (OT).
An OT is an array of addresses that map to your linked list, in which you can add entries
to the table based on the order you want commands sent/items drawn by the GPU. In
order to replace or insert a new entry, you can use the OT to look up what address
to replace the new entry with based on its index. Data in the table is drawn from
lowest index to highest index \cite{Everything}.

Another use of Order Tables is to act as a Z buffer in 3D drawing, as the PlayStation\textsuperscript{\textregistered}
lacks a hardware equivalent for this. Here, each entry in the OT represents a Z
index of its corresponding data packet. Because it is more common to have
higher Z values drawn first, and the GPU draws low OT entries first,
SONY added a DMA channel to automatically create a reversed OT for this method 
\cite{Everything}.

%----------------------------------------------------------------------------------------

\subsection{SPU}
\label{spusection}
The Sound Processing Unit (SPU) is the chip that generates audio for the
PlayStation\textsuperscript{\textregistered}. Audio on the PlayStation\textsuperscript{\textregistered} was a priority for SONY, so much
so that some describe the system as having audiophile level quality 
\cite{zigmahornet}\cite{musicalmusings}. As mentioned in \ref{sysoverview}, 
the BIOs of the PSX will
act as a CD player if an audio CD is inserted, complete with track
selection and visuals as shown in figure \ref{fig:cdplayer}.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{images/cdplayer.jpeg}
\caption{PSX Bios CD Player \cite{cdplayerscreen}}
\label{fig:cdplayer}
\end{figure}

There are 24 channels in the SPU (called 'voices') available which
can each play their own sounds simultaneously. These channels can be 
used to play audio samples, generate noise, or modulate the frequency of 
the following voice \cite{Everything}. The SPU has a 512 kilobyte buffer
for storing audio samples, meaning there isn't room for lengthy songs.
The PSX is also capable of streaming CD-Audio straight from disk, using 
the \textit{XA} file format (specific to the PlayStation\textsuperscript{\textregistered}). This would
be the preferred method for background music as it doesn't rely on the
audio file being less than 512 KB. In addition, the following effects
can be applied to each voice \cite{hitmen}:

\begin{itemize}
\item Attack Rate
    \begin{itemize}
        \item How fast the volume increase from zero to maximum
    \end{itemize}
\item Decay Rate
    \begin{itemize}
        \item How fast the volume decrease to sustain level
    \end{itemize}
\item Sustain Level
    \begin{itemize}
        \item The base level volume for holding the note
    \end{itemize}
\item Sustain Rate
    \begin{itemize}
        \item How fast the sustained note increases or decreases volume
    \end{itemize}
\item Release Rate
    \begin{itemize}
        \item How fast the note volume decreases to zero when turned off
    \end{itemize}
\end{itemize}

