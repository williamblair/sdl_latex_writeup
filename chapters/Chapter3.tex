% Chapter Template

\chapter{PSXSDK} % Main chapter title

\label{Chapter3} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

%----------------------------------------------------------------------------------------
%	SECTION 1 - PSXSDK
%----------------------------------------------------------------------------------------

\section{About}

PSXSDK is an open source library for PlayStation\textsuperscript{\textregistered} 1 development written
in C (although it has primitive C++ support also). To compile the library,
you must have a C compiler targeting the MIPS architecture, namely
\textit{mipsel-unknown-elf}. It was written by Giuseppe Gatta in 2009, 
with the most recent update being from January 2018 \cite{PSXSDK}.
You can find it online at \url{http://unhaut.x10host.com/psxsdk/}.
There are also prebuilt versions for Linux and Windows. However, the
Windows version is pretty outdated (from 2010) but can be downloaded
from the Google Code archive: 
\url{https://code.google.com/archive/p/psxsdk/downloads}.

\section{Installation}

For this project, I decided to compile the library from scratch, that 
way I could use any compiler version that would work with the SDL
source code. The following steps were used to install the SDK,
following the toolchain instructions in the PSXSDK source \cite{PSXSDK}:

\begin{enumerate}
    \item Download and Install Binutils

    Binutils version 2.31, configured and installed with:

    \begin{lstlisting}[breaklines=true]
./configure --prefix=/usr/local/psxsdk \
    --target=mipsel-unknown-elf --with-float=soft
make
sudo make install
    \end{lstlisting}

    \item Download and Install the Gnu C Compiler (GCC)

    GCC version 7.2.0, configured and installed with:

    \begin{lstlisting}[breaklines=true]
mkdir psx_gcc_build && cd psx_gcc_build
/path/to/gcc/configure --disable-libada --disable-libssp\
    --target=mipsel-unknown-elf --prefix=/usr/local/psxsdk \
    --with-float=soft --disable-nls --disable-libstc++v3 \
    --disable-libstdc__-v3 CFLAGS=-std=c99 \ 
    --enable-languages=c,c++
    \end{lstlisting}

    \item Download and Install PSXSDK

    PSXSDK release 20180115, configure Makefile.cfg:

    \begin{lstlisting}
    MAKE_COMMAND = make
    TOOLCHAIN_PREFIX=/usr/local/psxsdk
    EXAMPLES_VMODE=VMODE_NTSC
    \end{lstlisting}

    Then compile and install (assuming BASH shell is used):

    \begin{lstlisting}
    export PATH=$PATH:/usr/local/psxsdk/bin
    make
    sudo make install
    \end{lstlisting}

\end{enumerate}

\section{Usage}

\lstinputlisting[
    language=C,
    caption=An example application using PSXSDK (included with the windows version of the SDK),
    frame=single,
    label={lst:hellopsxsdk}]{code/testpsxsdk.c}

Listing \ref{lst:hellopsxsdk} shows a simple application utilizing psxsdk.
The functions \textit{PSX\_Init()}, \textit{GsInit()}, and \textit{GsClearMem()}
are used to initialize the system and graphics portions of the API. 
\textit{GsSetList()} sets the buffer used to create the linked list of graphics to
sent to the GPU via DMA, as described in section \ref{gpuoverview}.
\textit{GsSetVideoMode()} takes in the width, height, and display mode (PAL or NTSC)
used by the GPU. \textit{GsLoadFont()} loads the PlayStation\textsuperscript{\textregistered} BIOs font as a texture
into VRAM. Its arguments are the X and Y coordinates in VRAM to load the texture
and its CLUT (so here, the texture is stored at 768,0px and the CLUT is stored at
768,256px). \textit{SetVBlankHandler()} gives the API the function to call every
Vertical Blank (VBlank) of the PlayStation\textsuperscript{\textregistered}, meaning the PSX is done drawing
the current frame to the television. The \textit{if} statement in the main 
\textit{while} loop prevents us from trying to draw during a VBlank,
since the variable \textit{display\_is\_old} is updated by \textit{prog\_vblank\_handler}.
The \textit{dbuf}, \textit{GsSetDispEnvSimple()}, and \textit{GsSetDrawEnvSimple()}
statements are used to swap drawing and display buffers - if \textit{dbuf} is 1,
then our drawing coordinates will be 0,0px and our display coordinates 
will be 0,256px, and vice versa if \textit{dbuf} is 0. The functions \textit{GsSortCls()}
and \textit{GsPrintFont()} clear the screen with the given R,G,B color combination,
and draw text on the screen using the BIOs font at the given coordinates, respectively.
\textit{GsDrawList()} sends the linked command list set in \textit{GsSetList()} to the GPU
via DMA to be drawn. \textit{while(GsIsDrawing())} will loop until the GPU is done reading
and executing commands. Finally, we set \textit{display\_is\_old} to 0 to prevent
drawing again until the end of the next VBlank is signaled via \textit{prog\_vblank\_handler}.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{images/hello_psx.png}
\caption{Example PSXSDK program running on a PlayStation\textsuperscript{\textregistered}}
\label{fig:hello_psx}
\end{figure}

The result of the example program is shown in \ref{fig:hello_psx}, and can be compiled with

\begin{lstlisting}
psx-gcc hello.c -o hello.elf
elf2exe hello.elf -o hello.exe
mkdir cd_root
cp hello.exe cd_root
systemcnf main.exe > cd_root/system.cnf
mkisofs -o main.hfs -V MAIN -sysid PLAYSTATION cd_root
mkpsxiso main.hfs main.bin /usr/local/psxsdk/share/licenses/infousa.dat
\end{lstlisting}

\textit{psx-gcc} is a shell script which calls the cross compiler with the appropriate arguments:

\begin{lstlisting}[breaklines=true]
#!/bin/sh
mipsel-unknown-elf-gcc -D__PSXSDK__ -fno-strict-overflow \
    -fsigned-char -msoft-float -mno-gpopt -fno-builtin -G0 \
    -I/usr/local/psxsdk/include \
    -T/usr/local/psxsdk/mipsel-unknown-elf/lib/ldscripts/playstation.x $*
\end{lstlisting}

and is automatically installed as part of PSXSDK. \textit{elf2exe} and \textit{systemcnf} are
also installed as part of the SDK. After converting the \textit{elf} file to an \textit{exe}
file, the remaining commands are used to create a valid CD-ROM image, which
could be burned to a CD and ran on a PlayStation\textsuperscript{\textregistered}. The directory cd\_root
will be used to store all of the files we want in the CD image. The \textit{systemcnf}
command is used to create a SYSTEM.CNF file as described in \ref{sysoverview}
After all of your desired files are in the cd\_root directory, the \textit{mkisofs} command
(specific to Linux) creates an ISO file system from the given directory. Finally, the 
\textit{mkpsxiso} command (comes with PSXSDK) modifies the ISO FS to contain
the correct headers for a PlayStation\textsuperscript{\textregistered} CD image.


% sprites and primitives

\subsection{Primitives and Sprites}
The PlayStation\textsuperscript{\textregistered} has hardware support for drawing textured sprites and primitives (such
as rectangles). These are done through PSXSDK via a set of structures containing 
primtive or sprite information (such as location and size) and a set of related 
functions for adding those objects to the linked DMA list, as
exemplified below (taken from \textit{primitiv.c}, included with the Windows
version of PSXSDK \cite{PSXSDK}):

\begin{figure}[h]
\begin{lstlisting}[frame=single]
GsLine line;
line.r = 255; line.g = 255; line.b = 255;
line.x[0] = 10; line.y[0] = 15;
line.x[1] = 200; line.y[1] = 35;
line.attribute = 0;
GsSortLine(&line);
\end{lstlisting}
\caption{Example drawing a line using PSXSDK.}
\label{fig:gsline}
\end{figure}

Here, the function \textit{GsSortLine()} takes in a pointer to a \textit{GsLine}
struct, and adds the relevant information to the DMA list set by 
\textit{GsSetList()} earlier.

The idea for textured sprites is the same, just with more information:

\begin{figure}[h]
\begin{lstlisting}[frame=single]
GsSprite sprite;
sprite.x = 10; sprite.y = 10;
sprite.w = 100; sprite.h = 100;

sprite.r = 128; sprite.g = 128;	sprite.b = 128;

sprite.tpage = 5;
sprite.u = 0; sprite.v = 0;

sprite.cx = 0; sprite.cy = 0;

sprite.scalex = 1; sprite.scaley = 1;
sprite.mx = (screen_sprite.w/2) * (screen_sprite.scalex / SCALE_ONE);
sprite.my = (screen_sprite.h/2) * (screen_sprite.scaley / SCALE_ONE);
sprite.rotate = 0;

sprite.attribute = COLORMODE(COLORMODE_16BPP);
\end{lstlisting}
\caption{Example setting up a \textit{GsSprite} for drawing using PSXSDK.}
\label{fig:drawsprite}
\end{figure}

The \textit{r},\textit{g}, and \textit{b} components 
represent kind of the saturation of each color, from range 0-255. For
example, above each color has an even saturation of 128, so
the colors in the texture will look like normal. However, if you
were to set the \textit{r} component to 255 the texture
would have a red tint to its colors, or the colors would
be all black if each component was set to 0. \textit{tpage}
represents which texture page (starting from the top left
of vram) the texture for the sprite is loaded in. So, for
above, a texture page of 5 correlates to the location 320,0px
since each texture page is 64px wide. The \textit{u}
and \textit{v} members represent the offset of the texture
from the top left of the texture page, which is useful
for sprite clipping (when you want to use a specific area of the entire
texture to draw, and not the whole thing). \textit{cx} and \textit{cy}
represent the pixel coordinates in VRAM where the CLUT for that
texture is stored, which is only used if the texture is an 8 bit or 4 bit
image. \textit{mx} and \textit{my} are the coordinates, measured in PSX
scale units of 4096 (SCALE\_ONE) where the sprite is rotated around.
Finally, the \textit{attribute} member signifies what the BPP of 
the sprite is, so here we're saying its a 16bit sprite.

Although we have information about the sprite, we still need to load
the actual texture into VRAM. The most common way to do this is store
and load the texture in a TIM image, a format created by SONY
specifically for the PlayStation\textsuperscript{\textregistered} (taken from \textit{sprite.c},
included with the Windows version of PSXSDK \cite{PSXSDK}):

\begin{figure}[h]
\begin{lstlisting}[frame=single]
GsImage tim_image;
unsigned char data_buffer[0x40000]; // 256 kilobytes
FILE *f = fopen("cdrom:\\IMAGE.TIM;1", "rb");
...
fread(data_buffer, sizeof(char), fsize, f);
...
GsImageFromTim(&tim_image, data_buffer);
GsUploadImage(&tim_image);
GsSpriteFromImage(&sprite, &tim_image, 1);
\end{lstlisting}
\caption{Uploading and drawing a textured sprite using PSXSDK.}
\label{fig:uploadsprite}
\end{figure}

However, more useful for this project was the function
\textit{LoadImage()}, which lets you load a buffer of
data into VRAM directly:

\begin{lstlisting}[frame=single]
LoadImage(image_buffer, vram_x, vram_y, image_width, image_height);
\end{lstlisting}

Whose arguments are the image buffer to copy to VRAM, the coordinates in
VRAM to store the data, and the width and height of the image data,
which assumes the data is in 16 bit pixel format. This is important, as,
for example, if you were loading an 8 bit image instead of a 16 bit image,
the image width parameter would be half of the actual image width, since
there are two 8 bit pixels per one 16 bit VRAM value.

\subsection{Controllers/Events}

\begin{figure}[h]
\begin{lstlisting}[frame=single]
unsigned short padbuf = 0;
PSX_ReadPad(&padbuf, NULL);
if (padbuf & PAD_LEFT) {
    printf("Left being pressed!\n");
}
\end{lstlisting}
\caption{Reading the PlayStation\textsuperscript{\textregistered} controller using PSXSDK}
\label{fig:readcontroller}
\end{figure}

Listing \ref{fig:readcontroller} shows how you read the controller state
in PSXSDK. A 16bit buffer is used to store each button's status (1 or
0, in this case stored in \textit{padbuf}) and is read into via
\textit{PSX\_ReadPad()}. The second argument is to store the information
for the second controller if it is connected; here we send \textit{NULL}
so it is ignored. You can then check for specific button presses by
masking the 16 bit value with the appropriate API define like 
\textit{PAD\_UP}, \textit{PAD\_SQUARE}, etc. However, this method
doesn't allow you to read analog stick values, so for this project
we use an additional method. 

\begin{figure}[h]
\begin{lstlisting}[frame=single]
unsigned short padbuf = 0;
psx_pad_state pad_state;
PSX_PollPad(0, &pad_state);
padbuf = pad_state.buttons;

if (padbuf & pad_left) {
    printf("Left being pressed!\n");
}

printf("Left analog X axis value: %d\n",
        pad_state.extra.analogJoy.x[1]);

printf("Right analog Y axis value: %d\n",
        pad_state.extra.analogJoy.y[0]);
\end{lstlisting}
\caption{Reading the PlayStation\textsuperscript{\textregistered} controller using PSXSDK}
\label{fig:analogread}
\end{figure}

In listing \ref{fig:analogread} we use \textit{PSX\_PollPad()}
to store controller state information in a \textit{psx\_pad\_state}
structure. The first argument to \textit{PSX\_PollPad()} is the controller
number. From the struct, we can read \textit{padbuf} the same as in 
figure \ref{fig:readcontroller}, while additionally we get the
analog stick status in the \textit{x} and \textit{y} arrays of
\textit{analogJoy}. Index 0 refers to the right analog stick
and index 1 refers to the left analog stick. Their values will
be between -128 and 127. 

\subsection{Sound}
\label{psxsdksound}

Figure \ref{fig:rawsound} plays some sound data stored
in \textit{raw\_data} on voice channel 0 of the SPU. The code was 
taken from the \textit{snake} example which can be found with the
source of PSXSDK \cite{PSXSDK}.
\textit{raw\_data} is assumed to be an array of sound data
in \textit{raw} format, which can be generated by the program
\textit{wav2vag} which is included with PSXSDK.
Properties of the channel are set with the functions
\textit{SsVoiceStartAddr()}, \textit{SsVoiceVol()},
and \textit{SsVoicePitch()}. The SPU then plays the
channel via the \textit{SsKeyOn()} function. \textit{SsVoiceStartAddr()}
sets the memory location used to read sound data from for
the given voice, with \textit{SPU\_DATA\_BASE\_ADDR} defined
in PSXSDK. \textit{SsVoiceVol()} sets the left and right volume
for the given channel, with a max of 0x3FFF \cite{hitmen}.
\textit{SsVoicePitch()} allows you to offset the normal pitch
of the given voice, with a base of 0x1000. Octaves are sets of
512 units apart, e.g. a pitch of 3584 is one octave lower than normal
(0x1000-0x200=4096-512=3584) \cite{hitmen}. Ideally, for songs and
longer background music you would want to use CD-Audio streaming
but this isn't supported by PSXSDK. The sound will stop once it 
reaches the end of the data. Setting looping progammatically
isn't possible, as this is set by the sound file itself; this option
can be set by the \textit{wav2vag} program. To stop playing a channel
you call \textit{SsKeyOff()}.

\begin{figure}[h]
\begin{lstlisting}[frame=single]
SsInit();

SsUpload(raw_data, raw_data_length, SPU_DATA_BASE_ADDR);

const int channel = 0;
SsVoiceStartAddr(channel, SPU_DATA_BASE_ADDR);
SsVoiceVol(channel, 0x3FFF, 0x3FFF);
SsVoicePitch(channel, 0x1000 / (44100 / 11025));

SsKeyOn(channel);  // start playing
SsKeyOff(channel); // stop playing
\end{lstlisting}
\caption{Playing a raw sound byte on SPU channel 0 using PSXSDK}
\label{fig:rawsound}
\end{figure}

Besides the \textit{raw} format, PSXSDK also supports the \textit{MOD}
and \textit{VAG} sound formats. \textit{VAG} is a compressed sound file 
format specific to the PlayStation\textsuperscript{\textregistered} using Adaptive Differential Pulse
Code Modulation (ADPCM). As expected, these files can be generated
via \textit{wav2vag} as well. The \textit{MOD} file format is similar
to MIDI and is supported on the PlayStation\textsuperscript{\textregistered} via the \textit{libmodplay}
library within PSXSDK.

\begin{figure}[h]
\begin{lstlisting}[frame=single]
int game_music_loop = -1;
ModMusic *game_music = NULL;

MODSetBaseVoice(0);
MODSetMaxVolume(0x2fff);

game_music = MODLoad(filebuffer);
MODPlay(game_music, &game_music_loop);
\end{lstlisting}
\caption{Playing a MOD file}
\label{fig:modsound}
\end{figure}

Figure \ref{fig:modsound} is an example of how to play \textit{MOD}
sound files using libmodplay; the code was written by Giuseppe Gatta,
the creator of PSXSDK, for his game \textit{A Small Journey} 
\cite{asmalljourney}. \textit{filebuffer} is an array storing
the MOD file which was loaded from CD. Figure \ref{fig:playvag} on the
other hand shows how to play \textit{VAG} files. The sound information
is stored in a \textit{SsVag} structure via \textit{SsReadVag()}, where
again \textit{filebuffer} is the sound file loaded into memory. The
data is then automatically uploaded to the next location appropriate
in SPU memory via \textit{SsUploadVag()}, and played via 
\textit{SsPlayVag()}. The arguments to \textit{SsPlayVag()} are
a pointer to the \textit{vag} structure, which channel to use (voice),
and the left and right stereo volume to use.

\begin{figure}[h]
\begin{lstlisting}[frame=single]
SsVag vag;
int voice = 0;
SsInit();

SsReadVag(&vag, filebuffer);
SsUploadVag(&vag);

SsPlayVag(&vag, voice, 0x3FFF, 0x3FFF);
SsStopVag(&vag);
\end{lstlisting}
\caption{Playing a VAG file}
\label{fig:playvag}
\end{figure}

