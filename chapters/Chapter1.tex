% Chapter 1

\chapter{Simple DirectMedia Layer} % Main chapter title

\label{Chapter1} % For referencing the chapter elsewhere, use \ref{Chapter1} 

%----------------------------------------------------------------------------------------

% Define some commands to keep the formatting separated from the content 
\newcommand{\keyword}[1]{\textbf{#1}}
\newcommand{\tabhead}[1]{\textbf{#1}}
\newcommand{\code}[1]{\texttt{#1}}
\newcommand{\file}[1]{\texttt{\bfseries#1}}
\newcommand{\option}[1]{\texttt{\itshape#1}}

%----------------------------------------------------------------------------------------

\section{About/History}
Simple DirectMedia Layer (SDL) was created by Sam Lantinga in 1998, originally used to create a port of the popular video game
"Doom" to BeOS \cite{DoomFandom}. It is a wrapper API for operating system specific hardware such as video and audio,
and supports OpenGL and Direct3D contexts.
There are two major releases of SDL: 1.2 and 2.0. The latter version was
a major overhaul to 1.2, adding features such as hardware accelerated 2D and multiple windows \cite{SDLWiki}.
The library is split into multiple subsystems, each handling a different set of features\cite{SDLApi}:

\begin{itemize}
\item Timer
\item Audio
\item Video
\item Joystick

For controllers with joysticks

\item Haptic

Force feedback subsystem (for controllers)

\item Game Controller

Automatically initializes the joystick subsystem

\item Events

Events like key presses, window resizing, operating system signals, etc.

\item File I/O
\item Threading

Thread/Process creation and management

\end{itemize}

%----------------------------------------------------------------------------------------

\section{Usage}

\lstinputlisting[
    language=C,
    caption=An example SDL 1.2 application,
    label={fig:sdlapp},
    frame=single]{code/testsdl.c}

Listing \ref{fig:sdlapp} shows a simple application using SDL. All the
program does is create a blank application window, but the code exemplifies
the core layout each SDL program has. Each function or structure
in SDL begins with the \textit{SDL\_} prefix. The \textit{SDL\_Event} structure holds
information related to user or system input to the program, as part of the \textit{Events}
subsystem. A \textit{SDL\_Surface} structure holds information and pixel data of a
texture or image, which can be \textit{blitted} or copied onto other \textit{SDL\_Surfaces}
using the \textit{SDL\_BlitSurface()} function. \textit{SDL\_Init()} initializes specific SDL
subsystems - for example here we initialize the video subsystem by sending the 
\textit{SDL\_INIT\_VIDEO} flag. All SDL functions return a value less than 0 on error, so 
we check the return value of the function, and print an error message if so. 
\textit{SDL\_GetError()} returns a global string set by SDL, similar to \textit{errno}.
After initializing SDL, we then call \textit{SDL\_SetVideoMode()}, which returns 
a \textit{SDL\_Surface} pointer representing the application window or hardware
display, depending on the SDL implementation. How this is handled is up to a device
specific driver for each port of SDL.

Next, after initialization is done, we enter the main \textit{do/while} loop of the program.
We first check if any input has been sent to the application via \textit{SDL\_PollEvent()}.
This function stores information about a single event in a given \textit{SDL\_Event}
structure. We call it in a while loop as \textit{SDL\_PollEvent()} returns positive
every function call until there are no more events left in its queue. The event
type is then checked and can be handled by our application. Here, we check for a
\textit{SDL\_QUIT} event, which occurs when the operating system requests our application
to close (what happens when you click the 'X' at the top right of the application).
It is up to us, the programmer, to close our application ourselves; hence, we set
the \textit{quit} integer to 1 so we exit after reaching the bottom of the loop.

Outside of the event loop, \textit{SDL\_Flip} is called to tell the video subsystem
driver that we want to update our screen, sending it our \textit{SDL\_Surface} pointer
returned by \textit{SDL\_SetVideoMode()}. \textit{SDL\_Delay()} is called next to
prevent our program from running too fast, so we don't overwork the CPU. Its argument is
in milliseconds, e.g. an argument of 5000 would mean pause for 5 seconds.
Finally, we free the application window via \textit{SDL\_FreeSurface()} and close
all SDL subsystems with \textit{SDL\_Quit()}.

Listing \ref{fig:sdlapp} can be compiled on most systems with:
\begin{lstlisting}
gcc hellosdl.c -o hellosdl -lSDL
\end{lstlisting}

\subsection{Graphics}
\label{section:sdlgraphics}

Images in SDL can be drawn easily via the \textit{SDL\_image} extension library 
\cite{SDLimage}. However, the base SDL library includes the function 
\textit{SDL\_LoadBMP()}, which, as expected, loads a BMP image. It returns a
\textit{SDL\_Surface} pointer which can then be drawn via \textit{SDL\_BlitSurface()}.
To free the image you call \textit{SDL\_FreeSurface()} on it just as you
would with \textit{SDL\_SetVideoMode()} pointer. The term \textit{blitting} 
refers to when you copy an image
onto another image or surface, and the term \textit{clipping} means
only drawing part of a given image (think 'clipping' out a portion of the
image with scissors).

\begin{figure}[h]
\begin{lstlisting}[frame=single]
SDL_Surface *screen = SDL_SetVideoMode(WIDTH,HEIGHT,BPP,SDL_HWSURFACE);
SDL_Surface *image = SDL_LoadBMP("myimage.bmp");
SDL_Rect img_rect = {clip_x,clip_y,img_width,img_height};
SDL_Rect screen_rect = {img_x,img_y,img_width,img_height};
SDL_BlitSurface(image, &img_rect, screen, &screen_rect);
\end{lstlisting}
\caption{Drawing a BMP image in SDL.}
\label{fig:drawbmp}
\end{figure}

Figure \ref{fig:drawbmp} shows an example of loading and drawing a BMP image to
the screen. The \textit{SDL\_Rect} structures \textit{img\_rect} and \textit{screen\_rect}
provide information to \textit{SDL\_BlitSurface()} about where to draw the image 
and how large it is. \textit{img\_rect} represents the portion of the image to use for 
drawing - \textit{clip\_x} and \textit{clip\_y} represent the top left corner within
the image to draw, and \textit{img\_width}/\textit{img\_height} represent the number
of pixels wide and tall to use. \textit{screen\_rect} on the other hand contains
the x and y coordinates, as well as width and height, to place said image on the surface 
being drawn on (in this case \textit{screen}).

A visual example is shown in figure \ref{fig:cliprect}. The image on the left represents
the surface being blitted, and the box on the right represents the surface being blitted
to. The green rectangle would be \textit{img\_rect} in figure \ref{fig:drawbmp}, where
its top left are the coordinates \textit{clip\_x} and \textit{clip\_y}. The portion
of the image drawn on the right has coordinates \textit{img\_x} and \textit{img\_y}.
The blue arrow represents the copying done by \textit{SDL\_BlitSurface()}.

\begin{figure}[h]
\begin{centering}
\includegraphics[width=\textwidth]{images/lenna.png}
\end{centering}
\caption{SDL\_BlitSurface() after clipping.}
\label{fig:cliprect}
\end{figure}

\subsection{Events}
\label{sdlevents}

In listing \ref{fig:sdlapp}, only \textit{SDL\_QUIT} events are handled. Other 
SDL events include \textit{SDL\_KEYDOWN} and \textit{SDL\_KEYUP} for keyboard 
presses and releases; \textit{SDL\_MOUSEMOTION}, \textit{SDL\_MOUSEBUTTONDOWN},
and \textit{SDL\_MOUSEBUTTONUP} for mouse events; \textit{SDL\_JOYAXISMOTION},
\textit{SDL\_JOYBALLMOTION}, \textit{SDL\_JOYHATMOTION}, \textit{SDL\_JOYBUTTONDOWN},
and \textit{SDL\_JOYBUTTONUP} for game controller events; among others. For the
sake of this project, the \textit{SDL\_JOY}* events will be the most important
as we can use them with the PlayStation\textsuperscript{\textregistered}'s controllers.

\begin{figure}[h]
\begin{lstlisting}[frame=single]
while(SDL_PollEvent(&event))
{
    switch (event.type)
    {
        case SDL_JOYHATMOTION: 
            printf("Directional Button Event, value: %d\n", 
                    event.jhat.value); 
            break;
        case SDL_JOYBUTTONDOWN:
        case SDL_JOYBUTTONUP:
            printf("Button Press or Release Event, value: %d\n", 
                    event.jbutton.button);
            break;
        case SDL_JOYAXISMOTION:
            printf("Joystick motion: axis, value: ", 
                    event.jaxis.axis, event.jaxis.value);
            break;
        default: 
            printf("Other Event\n"); 
            break;
    }

}
\end{lstlisting}
\caption{Checking for controller events.}
\label{fig:checkhats}
\end{figure}

Figure \ref{fig:checkhats} first tracks controller directional button
presses or releases (directional buttons are referred to as 'hats'). 
\textit{event.jhat.value} will be one of 
\textit{SDL\_HAT\_CENTERED} for no buttons pressed, \textit{SDL\_HAT\_UP}...
\textit{SDL\_HAT\_DOWN} for a direction, or \textit{SDL\_HAT\_RIGHTUP}...
\textit{SDL\_HAT\_LEFTDOWN} for a diagonal direction. The \textit{JOYBUTTON}
event captures the index of the button pressed or released; the value of
which is dependant on the controller attached. This is so SDL can work with
any kind of controller with any number of buttons. Finally, the 
\textit{JOYAXISMOTION} event holds the value of a controller's joystick. Which
joystick and what axis it is (e.g. left joystick, vertical or right joystick, horizontal) 
is stored in \textit{event.jaxis.axis}, and its value will be from -32768 to
32768. A value of 0 means the joystick is centered, and for most joysticks you will
want to set a threshold for this value  otherwise your program will be overly sensitive
to joystick movement.

\subsection{Audio}

While SDL does offer an audio subsytem, it is much more common to use
an extension library built on top of SDL, named SDL\_Mixer 
\cite{sdlmixer}. The library basically
works in the following manner:
\begin{enumerate}
    \item Init mixer via \textit{Mix\_OpenAudio()}
    \item Load sound effects into structures called \textit{MIX\_Chunk}s
            via \textit{Mix\_LoadWAV()}.
                Confusingly, this function actually supports WAVE, 
                        AIFF, 
                        RIFF, OGG, and VOC files \cite{sdlmixer}.
    \item Load music into structures called \textit{Mix\_Music}s via
            \textit{Mix\_LoadMUS()}
    \item Play Chunks and Musics via \textit{Mix\_PlayMusic()} and
    \textit{Mix\_PlayChannel()}.
\end{enumerate}

The difference between \textit{Chunk}s and \textit{Music}s is that 
\textit{Chunk}s
are samples
which can be played simulatneously on different channels or voices,
like as described in chapter \ref{spusection}. \textit{Music} on the 
otherhand is
meant to stream background music and audio, of which only ONE song
may play at a time. This is understandable as why would you ever play
two songs at once?

Example usage of the library is shown in figure \ref{fig:mixerexample}.
The arguments to \textit{Mix\_OpenAudio()} are the sampling frequency in 
Hertz (which has a default of 22050), the output sample
format, the number of channels (so two means stereo output, one means
mono output), and finally the bytes used per output sample 
\cite{sdlmixer}. \textit{Mix\_PlayMusic()} takes in as its arguments
first the \textit{Mix\_Music} pointer to play, and then the number of 
times to play the audio, with -1 meaning loop forever \cite{sdlmixer}.
\textit{Mix\_PlayChannel()} on the other hand requires first, which
channel to play the sound on (NOT the same as the stereo/mono argument
previously), next the \textit{Mix\_Chunk} pointer to play, and then
how many times to repeat the sound (here 0 means no repeats, hence
play the sound once).

\begin{figure}[h]
\begin{lstlisting}[frame=single]
Mix_Music *music = NULL;
Mix_Chunk *chunk = NULL;

/* Init the SDL Audio Subsystem */
Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096)

/* Load audio into the music pointer */
music = Mix_LoadMUS("song.wav");

/* Load audio into the chunk pointer */
chunk = Mix_LoadWAV("effect.wav");

/* Play the music */
Mix_PlayMusic(music, -1);

/* Play the chunk */
Mix_PlayChannel(-1, chunk, 0);

/* Stop the music */
Mix_HaltMusic();
\end{lstlisting}
\caption{Example usage of the SDL\_Mixer library.}
\label{fig:mixerexample}
\end{figure}

%----------------------------------------------------------------------------------------

\section{Related Work}

The following are unofficial ports of SDL for various game consoles I have found
scattered around the web. It is possible that others exist.
I limited the list to "game consoles" as they most closely relate to the PSX.

\begin{itemize}

\item PlayStation\textsuperscript{\textregistered} 2
    
    Part of the open source PS2DEV/PS2SDK toolchain, as well as other useful ports 
    (SDL\_mixer, SDL\_gfx, SDL\_image, SDL\_ttf also ported) \cite{PS2}

\item PlayStation\textsuperscript{\textregistered} 3
    
    Targets SDL 1.2; there seem to be 2 main implementation branches: Cell SDK (SONY Official SDK)\cite{SDLGithub} and PSL1GHT 
    (Open Source SDK) \cite{PS3_l1ght} 

\item PlayStation\textsuperscript{\textregistered} Portable
    
    SDL 1.2 and 2.0 available \cite{SDLGithub}

\item GameBoy Advance \cite{NintendoDS}

\item Nintendo DS \cite{NintendoDS}

\item Nintendo 3DS \cite{3DS}

\item Dreamcast
    
    Also provides an OpenGL implementation\cite{Dreamcast}

\item Xbox \cite{Xbox}

\end{itemize}

%----------------------------------------------------------------------------------------
