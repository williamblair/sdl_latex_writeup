% Chapter Template

\chapter{Results} % Main chapter title

\label{Chapter5} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

%----------------------------------------------------------------------------------------
% Section 1 - Compiling and Test Environment
%----------------------------------------------------------------------------------------

\section{Compiling and Testing}

To match the usage of the library to that of other platforms, we
need a library object that can be compiled with programs via the
\textit{-lSDL} flag on GCC. This can be done with GCC by first
compiling the source files into .o object files, then combining
them using the \textit{ar} utility \cite{clibrary}:

\begin{lstlisting}
psx-gcc -c <source> -o <source name>.o
...
mipsel-unknown-elf-ar cru libSDL.a <object files>
\end{lstlisting}

The same was done for the sources for SDL\_Mixer, which is linked
with \textit{-lSDL\_mixer}, meaning the compiled library file needs to
be named \textit{libSDL\_mixer.a}.

To test applications on the PlayStation\textsuperscript{\textregistered}, it would be undesirable to
burn a CD every time you make a change to your application. 
One alternative method is to use an emulator.
This allows you to simulate the system on your computer and load the
compiled files directly from disk. The emulator I used for this 
project was NO\$PSX \cite{NOPSX}. NO\$PSX is more focused on being
a debugging tool than other emulators, and has a very useful VRAM viewer
, which was used to capture the images shown in this paper. 

The downside
to using an emulator is that you aren't running on actual hardware, 
which means there isn't a gurantee that your program will run exactly
the same as you tested it on the emulator. So, the second testing method
I used was to run on hardware using a PSIO \cite{PSIO}, which stands
for PlayStation\textsuperscript{\textregistered} Input Output. This is a 
device, shown in figure \ref{fig:psio},
that plugs into the parallel port of a PlayStation\textsuperscript{\textregistered} and runs games from
a SD card. Additionally, which is more useful and pertinent to this
project, you can run applications from a computer through the PSIO
via a usb cable. This is done by running the systems console, which you
download from the PSIO website, through which you select and upload
your application image \ref{fig:syscon}. The only downside to using
the systems console is that it is only available for Windows.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{images/psio.jpg}
\caption{PSIO device \cite{PSIO}. The bottom of the cartridge sticking
out plugs into the back of the PlayStation\textsuperscript{\textregistered} and you insert an SD card
into the top. The hole on the left is where you plug in the usb cable
to connect to a computer.}
\label{fig:psio}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics{images/syscon.png}
\caption{PSIO systems console \cite{PSIO}.}
\label{fig:syscon}
\end{figure}

\section{Example Application}
In order to test the resulting library, I wrote an application
which utilizes all of the functionality currently implemented. It is a
simple shooter game in which you control a space ship and fight an 
alien. There is both background music and sound effects, utilizing
the \textit{SDL\_Mixer} implementation. On the PlayStation\textsuperscript{\textregistered}, the
ship is controlled using the controller, testing the Events
subsystem and Joystick functions. Multiple 8 bit and 16 bit sprites
are loaded, and player/alien healthbars are drawn using 
\textit{SDL\_FillRect()} with multiple colors, testing the Video 
subsystem.

The application was compiled and run on both PC (Windows and Linux)
and on the PlayStation\textsuperscript{\textregistered} as a CD-ROM image. The only differences between
the code for the different platforms are the locations of files to load - 
that is, files on the PlayStation\textsuperscript{\textregistered} must be loaded with a 
\textit{cdrom:\textbackslash\textbackslash}
prefix - and the screen resolution, since 320x240 on the PlayStation\textsuperscript{\textregistered}
is very low compared to a normal PC screen resolution. The state
of the VRAM during runtime is given in figure \ref{fig:gamevram}, and
a screenshot of the running game is shown in figure \ref{fig:gamescreen}.

Part of the source for the test program is listed in figure 
\ref{fig:testappsource}, to hilight the differences between system
implementations. The preprocessor definition \textit{PSX\_BUILD} is
set if compiling for the PlayStation\textsuperscript{\textregistered}, and isn't set otherwise. The
code then checks for this declaration via \textit{\#ifndef}s, for example
to set a different screen width and height (\textit{S\_WIDTH} and 
\textit{S\_HEIGHT}) or to set what file to load (\textit{ship.bmp} or
\textit{cdrom:\textbackslash\textbackslash ship.bmp;1}).

As a result of writing the test application, I found the need for
implementations of the functions \textit{SDL\_SetColorKey()} and 
\textit{SDL\_Free*/Mix\_Free*}. \textit{SDL\_SetColorKey()} tells
SDL which color to make transparent during surface blitting. On
the PlayStation\textsuperscript{\textregistered} this isn't necessary as the color black (RGB 0,0,0)
is automatically treated as transparent. Similarly, the \textit{Free*}
set of functions isn't really required as it is expected that when
you exit your application, you just restart the PlayStation\textsuperscript{\textregistered}, so
freeing memory isn't really a concern. Therefore, both sets of functions
were implemented as dummy defines that don't do anything.

Another result of the test application was that a few bugs were 
discovered in the library. First, the texture page calculation in the 
Video subsystem was incorrect, as it did not account for the y 
coordinate of the texture location. So, any texture on the lower half
of the VRAM would not display as y was always assumed to be 0. Secondly,
the individual color extraction and calculation of 
\textit{SDL\_MapRGB()} was found to be incorrect as well. 
\textit{SDL\_MapRGB()} takes in individual RGB color values from 0 to 255
each and combines them into a single color value corresponding to the
supplied surface argument. In our case, this means it should return a 16 
bit PSX color value (5 bits B,G,R). Luckily, both of these bugs were
relatively minor and not overly difficult to fix.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{images/game_vram.png}
\caption{Test application resulting VRAM layout.}
\label{fig:gamevram}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{images/game_3.png}
\caption{Screenshot of the test application running on the PlayStation\textsuperscript{\textregistered}.}
\label{fig:gamescreen}
\end{figure}

\begin{figure}[h]
\begin{lstlisting}[frame=single,breaklines=true]
#include "SDL.h"
#include "SDL_mixer.h"
...
#ifndef PSX_BUILD
#define S_WIDTH  640
#define S_HEIGHT 480
#else
#define USE_JOYSTICK
#define S_WIDTH  320
#define S_HEIGHT 240
#endif
...
SDL_Event    event;
SDL_Surface *win_surface = NULL;
SDL_Joystick *joy1 = NULL;
...
/* Sprites and such */
int load_images(void)
{
    if (load_sprite(&ship.body, 
#ifndef PSX_BUILD
    "ship.bmp"
#else
    "cdrom:\\ship.bmp;1"
#endif
        ) < 0) return -1;
    SDL_SetColorKey(ship.body.surface, SDL_SRCCOLORKEY, SDL_MapRGB(win_surface->format, 0,0,0));
...
\end{lstlisting}
\caption{Part of the test application source code.}
\label{fig:testappsource}
\end{figure}

\section{Conclusion and Future Work}
In the current state of the port, we have a library which can handle
basic graphics, sound, and controller events on the PlayStation\textsuperscript{\textregistered}. As
exemplified by the application in the previous section, this is enough
to create simple games which utilize all aspects of the console. 
However, many features and subsystems are still missing from the 
implementation. Some features remain impossible due to the limitations
imposed by PSXSDK - for example, the threading and timer subsystems 
could not be implemented currently. The extension library 
\textit{SDL\_image}\cite{SDLimage} could be added for additional image format support,
and \textit{SDL\_ttf}\cite{SDLttf} could be added for true type font rendering.
Currently, both the Video and Audio subsystems have very basic memory
management. They simply fill memory at the next available 
space - there is no way to go back and free memory. A better solution
would be to implement the \textit{SDL\_Free*} family of functions
and utilize the space freed by them. This would allow for more complex
applications which have many more resources that would not all fit
into VRAM or sound RAM all at once. Additionally, as discussed in
chapter \ref{loadmus}, support for the \textit{VAG} and \textit{MOD}
audio formats could be added to the Audio subsystem. For the Video
subsystem, 4 bit and 24 bit BMP image support is currently missing.
Finally, for the Event subsystem, the second PSX controller port is
not supported at the moment either.
