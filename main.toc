\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Simple DirectMedia Layer}{7}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}About/History}{7}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Usage}{8}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.1}Graphics}{9}{subsection.1.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.2}Events}{10}{subsection.1.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.3}Audio}{11}{subsection.1.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Related Work}{12}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}The SONY PlayStation\textsuperscript {\textregistered } 1}{14}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}History}{14}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}System Overview/Hardware Specifications}{14}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}CPU}{15}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}MDEC}{15}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.3}GTE}{15}{subsection.2.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.4}GPU \& Video RAM}{16}{subsection.2.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.5}SPU}{17}{subsection.2.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}PSXSDK}{19}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}About}{19}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Installation}{19}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Usage}{20}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}Primitives and Sprites}{22}{subsection.3.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.2}Controllers/Events}{24}{subsection.3.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.3}Sound}{25}{subsection.3.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Implementation}{27}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Video}{28}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}SDL\_SetVideoMode()}{28}{subsection.4.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.2}Drawing Back End - Method One}{30}{subsection.4.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.3}Drawing Back End - Method Two}{33}{subsection.4.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.4}SDL\_LoadBMP()}{37}{subsection.4.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.5}SDL\_FillRect()}{42}{subsection.4.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Audio}{42}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Mix\_LoadMUS() and Mix\_LoadWAV()}{42}{subsection.4.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}Playing and Pausing Music}{44}{subsection.4.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Events and Joystick}{44}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.1}SDL\_PollEvent()}{44}{subsection.4.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.2}Joystick and Hats}{47}{subsection.4.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Results}{54}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Compiling and Testing}{54}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Example Application}{54}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Conclusion and Future Work}{56}{section.5.3}
