\documentclass[12pt,twoside]{report}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\graphicspath{{images/}}

\usepackage[a4paper,width=150mm,top=25mm,bottom=25mm,bindingoffset=6mm]{geometry}
\usepackage{fancyhdr}
\usepackage{nopageno} % maybe?
\usepackage{hyperref}


\usepackage{listings}
\lstset{language=C,basicstyle=\footnotesize}

%\usepackage[backend=bibtex,style=authoryear,natbib=true]{biblatex} % Use the bibtex backend with the authoryear citation style (which resembles APA)
\usepackage[backend=bibtex,style=ieee,natbib=true]{biblatex} % Use the bibtex backend with the authoryear citation style (which resembles APA)

\addbibresource{example.bib} % The filename of the bibliography

\usepackage{bytefield}
\usepackage{xcolor}

% https://tex.stackexchange.com/questions/6388/how-to-scale-a-tikzpicture-to-textwidth#6391
\usepackage{tikz}
\usetikzlibrary{decorations.markings}
\usepackage{environ}
\makeatletter
\newsavebox{\measure@tikzpicture}
\NewEnviron{scaletikzpicturetowidth}[1]{%
  \def\tikz@width{#1}%
  \def\tikzscale{1}\begin{lrbox}{\measure@tikzpicture}%
  \BODY
  \end{lrbox}%
  \pgfmathparse{#1/\wd\measure@tikzpicture}%
  \edef\tikzscale{\pgfmathresult}%
  \BODY
}
\makeatother

% https://tex.stackexchange.com/questions/163330/creating-figures-to-show-binary-values
\newcommand{\colorbitbox}[3]{%
\rlap{\bitbox{#2}{\color{#1}\rule{\width}{\height}}}%
\bitbox{#2}{#3}}
\definecolor{lightcyan}{rgb}{0.84,1,1}
\definecolor{lightgreen}{rgb}{0.64,1,0.71}
\definecolor{lightred}{rgb}{1,0.7,0.71}

\title{
    A Port of Simple DirectMedia Layer (SDL) v1.2 to the SONY PlayStation\textsuperscript{\textregistered}\textsuperscript{\textregistered}\textsuperscript{\textregistered} 1
}
%\author{William Blair III}
\author{
   %{\large SUNY Polytechnic Institute}
    %\vspace*{0.75in}
    {Submitted to the}\\
    {Graduate Faculty of the}\\
    {State University of New York Polytechnic Institute}\\
    {in Partial Fulfillment of the}\\
    {Requirements for the}\\
    {Degree of}\\
    \vspace*{0.75in}
    {Master of Science}
}

% prevent italicized words from extending past the right margin
% this makes lines not be as precise when aligning words to the right
% margin i guess
\sloppy

\begin{document}

%\maketitle
\thispagestyle{empty}
\begin{center}
{\Large A Port of Simple DirectMedia Layer (SDL) v1.2 to the SONY PlayStation\textsuperscript{\textregistered} 1}\\
\vspace*{0.75in}
{\large William Blair III}\\
\vspace*{0.45in}
{A Project}\\
\vspace*{0.65in}
\includegraphics[scale=0.5]{images/sunypolylogo.png}\\
\vspace*{0.55in}
{Submitted to the}\\
{Graduate Faculty of the}\\
{State University of New York Polytechnic Institute}\\
{in Partial Fulfillment of the}\\
{Requirements for the}\\
{Degree of}\\
\vspace*{0.75in}
{Master of Science}\\
\vspace*{0.75in}
{Utica, New York}\\
{\today}
\end{center}
\clearpage

% signature page
\thispagestyle{empty}
\begin{center}
{\Large A Port of Simple DirectMedia Layer (SDL) v1.2 to the SONY PlayStation\textsuperscript{\textregistered} 1}\\
\vspace*{0.25in}
{\large William Blair III}\\
\vspace*{0.5in}
{\large Department of Computer Science}
\end{center}
{Approved and recommended for acceptance as a project in partial fulfillment of the requirements for 
the degree of Master of Science in Computer and Information Science.}\\
\vspace*{0.5in}
\begin{flushright}
\noindent\begin{tabular}{ll}
\makebox[2.5in]{\hrulefill} & \makebox[2.5in]{\hrulefill}\\
Scott Spetka, Ph.D. & Date \\
Professor, Computer Science &
\\[8ex]% adds space between the two sets of signatures
\makebox[2.5in]{\hrulefill} & \makebox[2.5in]{\hrulefill}\\
Chen-Fu Chiang, Ph.D. & Date \\
Assistant Professor, Computer Science &
\\[8ex]
\makebox[2.5in]{\hrulefill} & \makebox[2.5in]{\hrulefill}\\
William "Amos" Confer, Ph.D. & Date \\
Assistant Professor, Computer Science &
\end{tabular}
\end{flushright}
\clearpage

\chapter*{Abstract}
The \textit{Simple DirectMedia Layer} library, commonly abbreviated to \textit{SDL}, is an application
programming interface (API) that acts as a wrapper for operating system-specific multimedia code \cite{SDL}. It is
capable of handling graphics, sound, and input events (such as keyboard and mouse input).
Originally written in 1998 by Sam Lantinga, SDL today officially runs on Windows, Mac OS X, 
Linux, iOS, and Android, making it an ideal choice for multi-platform support. 
In addition, implementations for other systems exist both within SDL's 
source code and other locations online.
For my project I provide a partial implementation of SDL for the SONY PlayStation\textsuperscript{\textregistered} 1, following
API specifications for version 1.2. This is accomplished using PSXSDK, an open source API for the 
PlayStation\textsuperscript{\textregistered} \cite{PSXSDK} with a MIPS 32 GNU GCC cross compiler. Current support includes part of the 
Graphics, Audio, and Event subsystems.

\iffalse
\chapter*{Dedication}
This project is dedicated to something

\chapter*{Declaration}
I declare something

\chapter*{Acknowledgements}
I would like to thank the PSX community for all the hacks, homebrew, and hardware. 
I can only hope to one day be as cool as you.
\fi

\tableofcontents

\pagestyle{fancy}
\rhead{\thepage}
\cfoot{}
\fancyhf{}
\fancyhead[LE,RO]{\thepage}
\fancyhead[RE]{\leftmark}
\fancyfoot{}

\chapter*{Introduction}
Portability is an important concept for programmers who write software
targeting multiple platforms. Device specific implementation means having
to rewrite the same code multiple times, forcing the developer to focus 
on details unrelated to the desired outcome of their application. 
Application Programming Interfaces (APIs) such as SDL relieve some
of this concern, allowing the developer to work on the higher level
logic of their software instead of the lower system level. My goal
for this project was to add another system implementation - the SONY
PlayStation\textsuperscript{\textregistered} 1 - to SDL. This provides another platform for which a
developer can add application support while requiring minimal 
modification of existing code. 

This paper is organized as follows: first, an overview of the SDL API
is provided, which shows how the outcome of the project should work and
look to the user on
the surface level. Next, a system description of the PlayStation\textsuperscript{\textregistered} is 
given. This is important for understanding how the PSXSDK API works and
what it does at the hardware level. After, PSXSDK is introduced to describe
what our SDL implementation will need to utilize behind the scenes.
Fourth, the SDL implementation is discussed, showing how the SDL and
PSXSDK APIs were integrated. Finally, we talk about the results of the
project, including a demo application using the API which targets
both desktop and the PlayStation\textsuperscript{\textregistered}.

% Chapters
\input{chapters/Chapter1.tex}
\input{chapters/Chapter2.tex}
\input{chapters/Chapter3.tex}
\input{chapters/Chapter4.tex}
\input{chapters/Chapter5.tex}

%\printbibliography[heading={bibintoc}]
\printbibliography

\end{document}
